#!/usr/bin/env python3

import datetime
import logging
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

import time
from optparse import OptionParser, OptionGroup

import fabric
from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.schedulers.background import BackgroundScheduler
from lofar.common.util import waitForInterrupt
from lofar.messaging import DEFAULT_BROKER, DEFAULT_BUSNAME
from lofar.messaging import UsingToBusMixin
from lofar.sas.tmss.client.tmssbuslistener import TMSSEventMessageHandler, TMSSBusListener
from lofar.sas.tmss.client.tmss_http_rest_client import TMSSsession

SCHEDULER = BackgroundScheduler(executors={'default': ThreadPoolExecutor(3)},
                                job_defaults={'coalesce': True, 'max_instances': 1})

logger = logging.getLogger(__name__)
RUNNING_JOB_IDS = {}
SBATCH_JOB_IDS = {}

DEFAULT_PROJECT = 'IDOLS'

def _to_datetime(timestamp):
    try:
        return datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
    except ValueError:
        return datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%f')


def find_observation(project, status) -> int:
    '''get the id of the latest TMSS subtask for the given project and status, or None if non-existant'''
    now = datetime.datetime.utcnow().replace(microsecond=0)
    one_day_before = now - datetime.timedelta(days=1)

    with TMSSsession.create_from_dbcreds_for_ldap("TMSSClient") as client:
        observations = client.get_subtasks(state=status,
                                           subtask_type='observation',
                                           project=project,
                                           scheduled_stop_time_greater_then=one_day_before,
                                           scheduled_start_time_less_then=now)

        if observations:
            observations = sorted(observations, key=lambda x: x['scheduled_start_time'], reverse=True)
            return observations[0]['id']
    return None


def get_task_information(subtask_id: int) -> dict:
    '''get the subtask json dict for the given id'''
    with TMSSsession.create_from_dbcreds_for_ldap("TMSSClient") as client:
        return client.get_subtask(subtask_id)


def is_idols_observation_in_right_project(subtask_id: int, project: str=DEFAULT_PROJECT):
    '''check if the subtask with the given subtask_id is an observation, from the correct project, and the correct strategy template'''
    subtask_info = get_task_information(subtask_id)
    if subtask_info.get('subtask_type','').lower()=='observation':
        if subtask_info.get('project', '').lower() == project.lower():
            with TMSSsession.create_from_dbcreds_for_ldap("TMSSClient") as client:
                task_blueprint = client.get_url_as_json_object(subtask_info['task_blueprint'])
                scheduling_unit_blueprint = client.get_url_as_json_object(task_blueprint['scheduling_unit_blueprint'])
                scheduling_unit_draft = client.get_url_as_json_object(scheduling_unit_blueprint['draft'])
                observation_strategy_template = client.get_url_as_json_object(scheduling_unit_draft['observation_strategy_template'])
                return 'idols' in observation_strategy_template['name'].lower()
    return False


def configure_logging(verbose=False):
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


def parse_args():
    parser = OptionParser(description='Monitor solar single station campaign')
    parser.add_option('-v', dest='verbose', action='store_true',
                      help='set logging to verbose')
    parser.add_option('-p', dest='project', default=DEFAULT_PROJECT, help='Project to filter on, default=%default')
    parser.add_option('-r', dest='recover', action='store_true')
    group = OptionGroup(parser, 'Messaging options')
    group.add_option('-b', '--broker', dest='broker', type='string',
                     default='localhost',
                     help='Address of the message broker, default: %default')
    group.add_option('-e', "--exchange", dest="exchange", type="string",
                     default=DEFAULT_BUSNAME,
                     help="Bus or queue where the QA notifications are published. [default: %default]")

    parser.add_option_group(group)
    (options, args) = parser.parse_args()

    return options


def check_if_slurm_task_is_still_running(connection, subtask_id, sjob_id):
    scmd = f'sacct -j {sjob_id} -n --format jobname%48,jobid,state -P'
    output = connection.run(scmd)

    interesting_output, *_ = filter(lambda x: x.startswith('solarmonitor'),
                                    output.stdout.split('\n'))

    job_name, job_id, state = interesting_output.strip().split('|')
    state = state.split(' ')[0]
    logging.info('Querying job id %s resulted in status line %s', job_id, output)
    if job_name != f'solarmonitor_{subtask_id}':
        logging.error('not corresponding job id', sjob_id)
        return False

    if state in ['PENDING', 'RUNNING']:
        return True


def generate_live_preview(subtask_id, ignore_finished=True, project=DEFAULT_PROJECT):
    task_info = get_task_information(subtask_id)
    end_time = _to_datetime(task_info['scheduled_stop_time'])
    is_too_late = end_time < datetime.datetime.now() and ignore_finished
    if not is_too_late and is_idols_observation_in_right_project(subtask_id, project):
        if subtask_id in RUNNING_JOB_IDS:
            del RUNNING_JOB_IDS[subtask_id]

            try:
                SCHEDULER.remove_job(RUNNING_JOB_IDS[subtask_id])
            except:
                pass

        if subtask_id in SBATCH_JOB_IDS:
            del SBATCH_JOB_IDS[subtask_id]


    logger.info(f'Connecting for {subtask_id}')
    slurm_id = None
    logging.info('Current job list %s', SBATCH_JOB_IDS)
    if subtask_id in SBATCH_JOB_IDS:
        slurm_id = SBATCH_JOB_IDS[subtask_id]

    with fabric.Connection('head.cep4.control.lofar') as connection:
        with connection.cd('/data/home/lofarsys/solar_test'):

            if slurm_id:
                is_job_running = check_if_slurm_task_is_still_running(connection,
                                                                      subtask_id, slurm_id)
                if is_job_running:
                    return
                else:
                    if subtask_id in SBATCH_JOB_IDS:
                        del SBATCH_JOB_IDS[subtask_id]

            cmd = f'sbatch -J solarmonitor_{subtask_id} -o preview_{subtask_id}.stdout -e preview_{subtask_id}.stderr update_preview.sh {subtask_id}'
            logger.info("submitting sbatch command: %s", cmd)
            output = connection.run(cmd)
            SBATCH_JOB_IDS[subtask_id] = output.stdout.strip().split(' ')[-1]
            logger.info(f'Running docker command for {subtask_id} on {connection.host}:', output.stdout)


@SCHEDULER.scheduled_job('interval', minutes=1)
def health_ping():
    logger.debug('Scheduler working')


def handle_started_observation(subtask_id):
    generate_live_preview(subtask_id)

    job = SCHEDULER.add_job(generate_live_preview, trigger='interval', args=(subtask_id,),
                            minutes=1, id=str(subtask_id))
    RUNNING_JOB_IDS[subtask_id] = job


class SolarMonitorTMSSBusListener(TMSSBusListener):
    class SolarMonitorTMSSEventMessageHandler(UsingToBusMixin, TMSSEventMessageHandler):
        log_event_messages = True # used by TMSSBusListener logger
        project = ''

        def onSubTaskStatusChanged(self, id: int, status: str):
            super().onSubTaskStatusChanged(id, status)

            if status in ('started', 'finished'):
                if is_idols_observation_in_right_project(id, self.project):
                    if status == 'started':
                        self.onObservationStarted(id)
                    elif status == 'finished':
                        self.onObservationStarted(id)


        def onObservationFinished(self, subtask_id):
            logging.info('Observation %s FINISHED', subtask_id)
            if is_idols_observation_in_right_project(subtask_id, self.project):
                if subtask_id in RUNNING_JOB_IDS:
                    SCHEDULER.remove_job(RUNNING_JOB_IDS[subtask_id])
                    del RUNNING_JOB_IDS[subtask_id]
                if subtask_id in SBATCH_JOB_IDS:
                    del SBATCH_JOB_IDS[subtask_id]
            else:
                logging.debug('shoot not the right one')

        def onObservationStarted(self, subtask_id):

            logging.info('Observation %s STARTED', subtask_id)
            if is_idols_observation_in_right_project(subtask_id, self.project):
                handle_started_observation(subtask_id)
            else:
                logging.info('shoot %s is not the right one', subtask_id)

    def __init__(self, project: str, exchange: str = DEFAULT_BUSNAME, broker: str = DEFAULT_BROKER):
        SolarMonitorTMSSBusListener.SolarMonitorTMSSEventMessageHandler.project = project
        super().__init__(
            handler_type=SolarMonitorTMSSBusListener.SolarMonitorTMSSEventMessageHandler,
            exchange=exchange,
            num_threads=1,
            broker=broker)


def main():
    options = parse_args()
    configure_logging(options.verbose)
    running_obs_id = find_observation(options.project, 'started')

    if running_obs_id:
        handle_started_observation(running_obs_id)

    if options.recover:
        finished_obs_id = find_observation(options.project, 'finished')

        if finished_obs_id:
            generate_live_preview(finished_obs_id)

    SCHEDULER.start()
    with SolarMonitorTMSSBusListener(options.project, exchange=options.exchange, broker=options.broker):
        waitForInterrupt()
    SCHEDULER.shutdown()


if __name__ == '__main__':
    main()
