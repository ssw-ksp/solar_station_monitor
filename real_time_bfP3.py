#! /usr/bin/env python
import sys

import numpy
from argparse import ArgumentParser
import os
import io
import h5py
import matplotlib.pyplot as plt
import numpy as np
import casacore.tables as pt
import matplotlib.dates as mdates
from datetime import datetime as dt
import logging

parser = ArgumentParser("create dynspectrum plot from raw data");
parser.add_argument('-i', '--rawdata', nargs='+', help='list of rawdata files', dest="rawfile", required=True)
parser.add_argument('-k', '--skip_samples', help='number of samples to skip', dest='skip_samples', type=int, default=0)
parser.add_argument('-s', '--vmin', help='vmin of plot', dest='vmin', type=float, default=0.5)
parser.add_argument('-m', '--vmax', help='vmax of plot', dest='vmax', type=float, default=2)
parser.add_argument('-c', '--cmap', help='matplotlib colormap', dest='cmap', default="viridis")
parser.add_argument('-n', '--show_normalization', help='plot normalization', dest='show_normalization',
                    action='store_true')
parser.add_argument('-o', '--output_plot', help='output plot', dest='output_plot')

# To DO: fix complex_voltages plotting (make sure they start at the same time). Save as png. Do not keep full buffer in memory. Get info from h5files. Maybe keep x-axis fixed. # plotmedian #add axes etc #find parset# figure ut how to stop the plotting

bytes_per_sample = 4
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

def get_metadata_from_h5(h5file):
    # metadata=h5file.attrs[u'NOF_SUB_ARRAY_POINTINGS']
    metadata = dict(h5file[h5file.visit(lambda x: x if 'STOKES' in x else None)].attrs)
    metadata['freqs'] = h5file[h5file.visit(lambda x: x if 'COORDINATE_1' in x else None)].attrs[u'AXIS_VALUES_WORLD']
    metadata = dict(metadata, **dict(h5file[h5file.visit(lambda x: x if 'BEAM' in x else None)].attrs))
    metadata["starttime"] = h5file.attrs[u'OBSERVATION_START_MJD']
    metadata["endtime"] = h5file.attrs[u'OBSERVATION_END_MJD']
    return metadata


def plot_real_time(fig, axarr, rawfile, nch, nSB, freqs, vmin, vmax, maxSamples=10000, skiptime=25, skipch=1,
                   skipSamples=0, starttime=None, endtime=None, sampleSize=1. / 125., cmap='Reds', show_norm=False,
                   output_plot='realtime.png'):
    hasdata = False
    rawfile.seek(0, io.SEEK_END)
    length_file = rawfile.tell()
    rawfile.seek(0, 0)
    rawfile.seek(skipSamples * bytes_per_sample * nch * nSB)

    if starttime:
        starttime += (skipSamples * sampleSize) / (24 * 3600.)
        starttime_dt = dt.strptime(pt.taql('select str(mjdtodate({})) as date'.format(starttime)).getcol('date')[0],
                                   '%Y/%m/%d/%H:%M:%S')
        fig.suptitle(starttime_dt.strftime("%m/%d/%Y"))
    bytes_per_iteration = (maxSamples * bytes_per_sample * nch * nSB)
    n_of_iterations = int(length_file // bytes_per_iteration)
    tmp_averaged_data = os.path.join(os.path.dirname(output_plot), 'averaged_data.npz')

    if os.path.exists(tmp_averaged_data):
        try:
            tmp_data = numpy.load(tmp_averaged_data)
            start_index = tmp_data['index']
            data = tmp_data['data']
            hasdata = True
            for i in range(start_index):
                rawfile.seek(bytes_per_iteration, io.SEEK_CUR)
        except Exception as e:
            logging.warning('There was an exception but it is recovering it')
            logging.exception(e)
            os.remove(tmp_averaged_data)
            start_index = 0
    else:
        start_index = 0
    for i in range(start_index, n_of_iterations):
        mybuffer = rawfile.read(maxSamples * bytes_per_sample * nch * nSB)
        tmpdata = np.frombuffer(mybuffer, dtype=np.float32)  # np.float = np.float64!!
        nSam = tmpdata.shape[0] / (nch * nSB)
        print('processing sample', i)
        if not hasdata:

            data = tmpdata.reshape(int(nSam), (nch * nSB))[::skiptime, ::skipch]
            hasdata = True
        else:
            data = np.concatenate((data, tmpdata.reshape(int(nSam), (nch * nSB))[::skiptime, ::skipch]), axis=0)
        mymedian = np.median(data, axis=0)

        if i != 0 and (i % 1000 == 0):
            numpy.savez(tmp_averaged_data, index=i, data=data)
        # fig.clf()
        ax = axarr
        if show_norm:
            ax = ax[0]
        ax.cla()
        myextent = [0, data.shape[0], freqs[0] * 1e-6, freqs[::skipch][-1] * 1e-6]
        if not (starttime is None):
            myextent[0] = mdates.date2num(starttime_dt)
            myextent[1] = mdates.date2num(dt.strptime(pt.taql('select str(mjdtodate({})) as date'.format(
                starttime + (data.shape[0] * skiptime * sampleSize) / (24. * 3600.))).getcol('date')[0],
                                                      '%Y/%m/%d/%H:%M:%S'))  # thanks to TJD
            ax.imshow((data / mymedian).T, origin='lower', interpolation='nearest', aspect='auto', vmin=vmin, vmax=vmax,
                      extent=myextent, cmap=cmap)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        ax.set_ylabel("freq (MHz)")
        if show_norm:
            ax = axarr[1]
            ax.cla()
            ax.plot(freqs[::skipch] * 1e-6, mymedian, 'k')
            ax.set_xlabel("freq (MHz)")
        # plt.pause(.3)

        plt.savefig(output_plot)


def plot_real_time_complex_voltages(rawfiles, nch, nSB, freqs, vmin, vmax, maxSamples=10000, skiptime=100, skipch=1):
    hasdata = False
    buffers = ['', '', '', '']
    prevminsize = 0
    while True:
        minsize = -1
        for i in range(len(rawfiles)):
            buffers[i] = buffers[i][prevminsize:] + rawfiles[i].read(maxSamples * bytes_per_sample * nch * nSB)
            # print minsize,len(buffers[i])
            if minsize < 0 or len(buffers[i]) < minsize:
                minsize = len(buffers[i])
        prevminsize = minsize
        tmpdata = np.frombuffer(buffers[0][:minsize], dtype=np.float32)
        nSam = tmpdata.shape[0] / (nch * nSB)
        print("reshaping", tmpdata.shape[0], "to", nSam, nch * nSB, minsize)
        tmpdata = tmpdata.reshape(nSam, (nch * nSB))[::skiptime, ::skipch]
        tmpdata = tmpdata ** 2 + \
                  np.frombuffer(buffers[1][:minsize], dtype=np.float32).reshape(nSam, (nch * nSB))[::skiptime,
                  ::skipch] ** 2 + \
                  np.frombuffer(buffers[2][:minsize], dtype=np.float32).reshape(nSam, (nch * nSB))[::skiptime,
                  ::skipch] ** 2 + \
                  np.frombuffer(buffers[3][:minsize], dtype=np.float32).reshape(nSam, (nch * nSB))[::skiptime,
                  ::skipch] ** 2
        tmpdata = np.sqrt(tmpdata)
        if not hasdata:
            data = tmpdata
            hasdata = True
        else:
            data = np.concatenate((data, tmpdata), axis=0)
        mymedian = np.median(data, axis=0)
        plt.clf()
        plt.subplot(2, 1, 1)
        plt.imshow((data / mymedian).T, origin='lower', interpolation='nearest', aspect='auto', vmin=vmin, vmax=vmax,
                   extent=(0, data.shape[0], freqs[0] * 1e-6, freqs[::skipch][-1] * 1e-6))
        plt.ylabel("freq (MHz)")
        plt.subplot(2, 1, 2)
        plt.plot(freqs[::skipch] * 1e-6, mymedian, 'k')
        plt.xlabel("freq (MHz)")
        plt.pause(.1)
        plt.savefig("fig.png")


def main(argv):
    args = parser.parse_args(argv)
    skipSamples = args.skip_samples
    rawfiles = [open(i, 'rb') for i in args.rawfile]
    metadata = get_metadata_from_h5(h5py.File(args.rawfile[0].replace('.raw', '.h5')))
    # a=[rawfile.seek(-1000*60*16*4,os.SEEK_END) for rawfile in rawfiles]
    # buffers=[rawfile.read(1000*400*1*4) for rawfile in rawfiles]
    if not metadata[u'COMPLEX_VOLTAGE']:
        for i, rawfile in enumerate(rawfiles):
            fig, axarr = plt.subplots(1 + args.show_normalization, 1)
            plot_real_time(fig, axarr, rawfile, metadata['CHANNELS_PER_SUBBAND'], metadata[u'NOF_SUBBANDS'],
                           metadata['freqs'], args.vmin, args.vmax, skipSamples=skipSamples,
                           starttime=metadata['starttime'], endtime=metadata['endtime'],
                           sampleSize=metadata[u'SAMPLING_TIME'], cmap=args.cmap, show_norm=args.show_normalization,
                           output_plot=args.output_plot)
    else:
        for i in range(len(rawfiles))[::4]:
            rawfile4 = rawfiles[i * 4:i * 4 + 4]
            fig, axarr = plt.subplots(2, 1)
            plot_real_time_complex_voltages(fig, axarr, rawfile4, metadata['CHANNELS_PER_SUBBAND'],
                                            metadata[u'NOF_SUBBANDS'], metadata['freqs'], args.vmin, args.vmax,
                                            output_plot=args.output_plot)


if __name__ == '__main__':
    main(sys.argv[1:])
